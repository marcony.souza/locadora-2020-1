package br.ucsal.bes20201.testequalidade.locadora;

import static org.junit.jupiter.api.Assertions.assertEquals;

import br.ucsal.bes20201.testequalidade.locadora.business.*;
import br.ucsal.bes20201.testequalidade.locadora.persistence.*;
import br.ucsal.bes20201.testequalidade.locadora.dominio.Veiculo;
import br.ucsal.bes20201.testequalidade.locadora.builder.*;
import java.util.ArrayList;
import java.util.List;
import org.junit.jupiter.api.Test;

public class LocacaoBOIntegradoTest {

	/**
	 * Testar o calculo do valor de loca��o por 3 dias para 2 veiculos com 1 ano de
	 * fabrica�ao e 3 veiculos com 8 anos de fabrica��o.
	 */
	
	private List<Veiculo> veiculos;

	@Test
	public void testarCalculoValorTotalLocacao5Veiculos3Dias() {
		
		veiculos = veiculos == null ? new ArrayList <Veiculo>() : veiculos;
		
		Veiculo v1 = VeiculoBuilder.veiculoDefault().withAno(2012).build();
		Veiculo v2 = VeiculoBuilder.veiculoDefault().withAno(2012).build();
		Veiculo v3 = VeiculoBuilder.veiculoDefault().withAno(2012).build();
		Veiculo v4 = VeiculoBuilder.veiculoDefault().withAno(2019).build();
		Veiculo v5 = VeiculoBuilder.veiculoDefault().withAno(2019).build();
		
		veiculos.add(v1);
		veiculos.add(v2);
		veiculos.add(v3);
		veiculos.add(v4);
		veiculos.add(v5);
		
		for(Veiculo veiculoAux : veiculos) {	
		VeiculoDAO.insert(veiculoAux);
		}
		
		assertEquals(3525.0, LocacaoBO.calcularValorTotalLocacao(veiculos, 3));		

	}

}
