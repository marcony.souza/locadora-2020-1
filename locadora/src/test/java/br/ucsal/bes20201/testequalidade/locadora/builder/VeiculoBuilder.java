package br.ucsal.bes20201.testequalidade.locadora.builder;

import br.ucsal.bes20201.testequalidade.locadora.dominio.Modelo;
import br.ucsal.bes20201.testequalidade.locadora.dominio.Veiculo;
import br.ucsal.bes20201.testequalidade.locadora.dominio.enums.SituacaoVeiculoEnum;

public class VeiculoBuilder {

	public final static Modelo DEFAULT_MODELO = new Modelo("Ford-GT");
	public final static Integer DEFAULT_ANO = 2008;
	public final static Double DEFAULT_VALOR_DIARIA = 250.00;
	public final static String DEFAULT_PLACA = "IAJ5397";
	public final static SituacaoVeiculoEnum DEFAULT_SITUACAO = SituacaoVeiculoEnum.DISPONIVEL;

	private Modelo modelo;
	
	private Integer ano;
	
	private Double valorDiaria;
	
	private String placa;
	
	private SituacaoVeiculoEnum situacao;

	private VeiculoBuilder() {}

	public static VeiculoBuilder builderDeVeiculo() {
		return new VeiculoBuilder();
	}

	public VeiculoBuilder withPlaca(String placa) {
		this.placa = placa;
		return this;
	}

	public VeiculoBuilder withAno(Integer ano) {
		this.ano = ano;
		return this;
	}

	public VeiculoBuilder withModelo(Modelo modelo) {
		this.modelo = modelo;
		return this;
	}

	public VeiculoBuilder withValorDiaria(Double valorDiaria) {
		this.valorDiaria = valorDiaria;
		return this;
	}

	public VeiculoBuilder withSituacao(SituacaoVeiculoEnum situacao) {
		this.situacao = situacao;
		return this;
	}

	public VeiculoBuilder withSituacaoDisponivel() {
		this.situacao = SituacaoVeiculoEnum.DISPONIVEL;
		return this;
	}

	public VeiculoBuilder withSituacaoLocado() {
		this.situacao = SituacaoVeiculoEnum.LOCADO;
		return this;
	}

	public VeiculoBuilder withSituacaoManutencao() {
		this.situacao = SituacaoVeiculoEnum.MANUTENCAO;
		return this;
	}

	public VeiculoBuilder but() {
		
		return builderDeVeiculo()
				.withModelo(modelo)
				.withAno(ano)
				.withValorDiaria(valorDiaria)
				.withPlaca(placa)
				.withSituacao(situacao);
	}

	public Veiculo build() {
		Veiculo veiculo = new Veiculo();
		
		veiculo.setModelo(this.modelo);
		veiculo.setAno(this.ano);
		veiculo.setValorDiaria(this.valorDiaria);
		veiculo.setPlaca(this.placa);
		veiculo.setSituacao(this.situacao);
		
		return veiculo;
	}


	public static VeiculoBuilder veiculoDefault() {
		
		return VeiculoBuilder.builderDeVeiculo()
				.withModelo(DEFAULT_MODELO)
				.withAno(DEFAULT_ANO)
				.withValorDiaria(DEFAULT_VALOR_DIARIA)
				.withPlaca(DEFAULT_PLACA)
				.withSituacao(DEFAULT_SITUACAO);
	}

}
